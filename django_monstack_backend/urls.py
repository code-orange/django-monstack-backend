from django.contrib import admin

from django.conf import settings
from django.urls import include, path
from django.urls import re_path

from django_monstack_notifier_api.django_monstack_notifier_api import (
    urls as notifier_api_urls,
)

urlpatterns = [
    re_path(r"", include("user_sessions.urls", "user_sessions")),
    path("admin/", admin.site.urls),
    re_path(r"^watchman/", include("watchman.urls")),
    path("notifier/api/", include(notifier_api_urls)),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns
